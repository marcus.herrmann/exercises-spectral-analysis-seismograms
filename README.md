# Exercises - Spectral analysis of seismograms

Python Jupyter notebooks about:
1. Fourier series, Fourier transform, and signals [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/marcus.herrmann%2Fexercises-spectral-analysis-seismograms/HEAD?labpath=exercise1-Fourier%2BSignals.ipynb)
2. Spectral analysis of seismograms  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/marcus.herrmann%2Fexercises-spectral-analysis-seismograms/HEAD?labpath=exercise2-Seismograms%2BSpectralAnalysis.ipynb)

<br>

**To execute and work with them _interactively_** (instead of only showing their content), **click <img style="filter: saturate(25%);" src="https://mybinder.org/badge_logo.svg"> above**.

<br>

---

Copyright © 2022 Marcus Herrmann — Università degli Studi di Napoli 'Federico II'

Licensed under the [European Union Public Licence](https://joinup.ec.europa.eu/collection/eupl) ([EUPL-1.2-or-later](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)), the first European Free/Open Source Software (F/OSS) license. It is available in all official languages of the EU.
